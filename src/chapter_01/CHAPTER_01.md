Chapter 1 — The Novice Standards
================================

*Being a curated slice of the South Carolina College- and Career-Ready (CCR) Standards for General Music Proficiency at the novice levels, adapted, with accompanying material.*

---
<img src=".kermit256.png" style="float:right;padding:1rem;padding-top:0px">

In 2017 the state of South Carolina approved new standards for music education.
The standards themselves are, in my humble but accurate opinion, *real good*.

Links to PDFs of the complete standards can be found [here](https://ed.sc.gov/instruction/standards-learning/visual-and-performing-arts/standards/),
with this caveat posted alongside:
"[we are] aware that the document is not fully accessible. We are in the process of correcting the issue and will post an accessible document as quickly as possible."

They are not exaggerating.
It is arranged in a bunch of tables that make it hard to navigate,
hard to copy and paste from,
and even harder to pipe through text-to-speech apps.
In other words, it's a mess.
Since for me listening to text is a necessary accommodation,
and since our new culture of online learning has made parents and students into teachers for themselves,
I share this with you.
