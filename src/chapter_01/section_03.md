1.3. Benchmarks and Indicators
==============================
*Goals and assessment criteria in each Standard for General Music at three divisions of the Novice Level.*

---

## Creating  
>**Creating** - I can use the elements of music to communicate new musical ideas and works.  

### Anchor Standard 1: I can arrange and compose music.  

I can imitate a musical statement by sight and sound. Benchmark GM.CR NL.1  
I can match sound and pattern. Indicator GM.CR NL.1.1  
I can imitate a given music rhythm or sound using symbols. Indicator GM.CR NL.1.2  

I can answer a musical question. Benchmark GM.CR NM.1  
I can identify simple forms. Indicator GM.CR NM.1.1  
I can identify same and different patterns. Indicator GM.CR NM.1.2  

I can arrange a musical idea. Benchmark GM.CR NH.1  
I can use rhythm patterns, songs or words to create a musical idea. Indicator GM.CR NH.1.1  
I can create a musical idea given specific instructions. Indicator GM.CR NH.1.2  

### Anchor Standard 2: I can improvise music.  

I can imitate simple rhythm patterns within a given meter. Benchmark GM.CR NL.2  
I can identify same and different rhythms patterns. Indicator GM.CR NL.2.1  
I can echo simple rhythm patterns. Indicator GM.CR NL.2.2  

I can imitate simple tonal patterns within a given key and tonality. Benchmark GM.CR NM.2  
I can identify same and different melodic patterns. Indicator GM.CR NM.2.1  
I can echo simple tonal patterns.   Indicator GM.CR NM.2.2  

I can improvise responses to given rhythmic patterns. Benchmark GM.CR NH.2  
I can improvise simple ostinati patterns within a given meter. Indicator GM.CR NH.2.1  
I can improvise rhythm patterns, songs or chants to create a musical idea. Indicator GM.CR NH.2.2  
## Performing  
>**Performing** - I can perform a variety of music with fluency and expression.  

### Anchor Standard 3: I can sing alone and with others.  
I can use my voice in many ways.  Benchmark GM.P NL.3  
I can sing songs in my range. Indicator GM.P NL.3.1  
I can use my voice to imitate other sounds. Indicator GM.P NL.3.2  

I can use my singing voice. Benchmark GM.P NM.3  
I can match pitch when I sing. Indicator GM.P NM.3.1  
I can sing with my head voice and chest voice. Indicator GM.P NM.3.2  

I can sing, chant, and move to demonstrate a steady beat. Benchmark GM.P NH.3  
I can sing or move using a steady beat. Indicator GM.P NH.3.1  
I can use good posture and breath support when I sing. Indicator GM.P NH.3.2  

### Anchor Standard 4: I can play instruments alone and with others.  

I can make sounds with classroom instruments and other sound sources. Benchmark GM.P NL.4  
I can use my body to make sounds. Indicator GM.P NL.4.1  
I can play pitched and unpitched instruments. Indicator GM.P NL.4.2  
I can follow the teacher when I use classroom instruments. Indicator GM.P NL.4.3  

I can imitate short rhythmic patterns. Benchmark GM.P NM.4  
I can echo an ostinato rhythm pattern. Indicator GM.P NM.4.1  
I can play melodic patterns using steps and skips. Indicator GM.P NM.4.2  
I can ask and answer musical questions using instruments.   Indicator GM.P NM.4.3  

I can play and read rhythmic, melodic, and chord patterns. Benchmark GM.P NH.4  
I can use music notation to play instruments. Indicator GM.P NH.4.1  
I can play pentatonic scales on instruments. Indicator GM.P NH.4.2  
I can identify rhythmic notation. Indicator GM.P NH.4.3  

### Anchor Standard 5: I can read and notate music.  
Benchmark GM.P NL.5: I can read rhythm patterns.  
Indicator GM.P NL.5.1: I can read rhythm patterns with my voice, body, and instruments.  
Indicator GM.P NL.5.2: I can read basic rhythms.  

Benchmark GM.P NM.5: I can read simple rhythmic and melodic notation.  
Indicator GM.P NM.5.1: I can name notes in treble clef.  
Indicator GM.P NM.5.2: I can read simple quarter, eighth, half, whole notes and rests.  

Benchmark GM.P NH.5: I can read, write simple rhythmic and melodic standard notation.  
Indicator GM.P NH.5.1: I can read standard notation.  
Indicator GM.P NH.5.2: I can read meter in 4/4, 3/4, and 2/4.  


## Responding  
>**Responding** - I can respond to musical ideas as a performer and listener.  
### Anchor Standard 6: I can analyze music.
Benchmark GM.R NL.6: I can identify contrasts in music.  
Indicator GM.R NL.6.1: I can identify dynamics and steady beat.  
Indicator GM.R NL.6.2: I can identify same and different sound sources.  
Indicator GM.R NL.6.3: I can name same and different sections.  

Benchmark GM.R NM.6: I can identify the elements of music.  
Indicator GM.R NM.6.1: I can identify changes in dynamics, tempo and rhythm.  
Indicator GM.R NM.6.2: I can name voice types and instrument families.  
Indicator GM.R NM.6.3: I can identify examples of some basic musical forms.  

Benchmark GM.R NH.6: I can describe how the music elements are used.  
Indicator GM.R NH.6.1: I can use appropriate vocabulary to describe pitch, tempo, and dynamics.  
Indicator GM.R NH.6.2: I can identify by sight and sound voice types and classroom instruments.  
Indicator GM.R NH.6.3: I can identify examples of complex musical forms.  

### Anchor Standard 7: I can evaluate music.
Benchmark GM.R NL.7: I can use my words to talk about music.  
Indicator GM.R NL.7.1: I can listen and respond to music.  

Benchmark GM.R NM.7: I can demonstrate how to be an audience member in different musical settings.  
Indicator GM.R NM.7.1: I can model and describe audience behavior in different settings.  

Benchmark GM.R NH.7: I can use musical vocabulary to describe personal preference choices.  
Indicator GM.R NH.7.1: I can talk and write about music using musical vocabulary.  

## Connecting  
>**Connecting** - I can relate music ideas to personal meaning, other arts disciplines, and content areas.  
### Anchor Standard 8: I can examine music from a variety of stylistic and historical periods and cultures.
Benchmark GM.C NL.8: I can recognize and perform musical selections from my own culture and some time periods.  
Indicator GM.C NL.8.1: I can recognize that all cultures and time periods use music.  

Benchmark GM.C NM.8: I can identify and perform musical selections from a culture other than mine and a historical time period.  
Indicator GM.C NM.8.1: I can find similar elements of music within a culture/time period.  

Benchmark GM.C NH.8: I can identify and perform musical selections from multiple cultures and/or historical time periods.  
Indicator GM.C NH.8.1: I can find similar elements of music in different cultures/time periods.  

### Anchor Standard 9: I can relate music to other arts disciplines, other subjects, and career paths.  
Benchmark GM.C NL.9: I can explore general music concepts among arts disciplines other content areas and related careers in familiar settings.  
Indicator GM.C NL.9.1: I can identify the relationship between music and another subject in my school.  
Indicator GM.C NL.9.2: I can identify topics in music that interest me.  

Benchmark GM.C NM.9: I can recognize general music concepts among arts disciplines, other content areas and related careers.  
Indicator GM.C NM.9.1: I can make connections between music and another subject in my school.  
Indicator GM.C NM.9.2: I can identify life skills necessary for a music career.  

Benchmark GM.C NH.9: I can apply general music concepts to arts disciplines, other content areas and related careers including South Carolina.  
Indicator GM.C NH.9.1: I can demonstrate and describe the relationship between music and a concept from another subject in my school.  
Indicator GM.C NH.9.2: I can identify specific careers in music.  
