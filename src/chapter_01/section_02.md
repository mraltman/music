1.2. Anchor Standards
=====================

*Guiding "Anchor" Standards for General Music.*

---


>❝ I can arrange and compose music.  
— Anchor Standard 1

>❝ I can improvise music.  
— Anchor Standard 2

>❝ I can sing alone and with others.  
— Anchor Standard 3

>❝ I can play instruments alone and with others.  
— Anchor Standard 4

>❝ I can read and notate music.  
— Anchor Standard 5

>❝ I can analyze music.  
— Anchor Standard 6

>❝ I can evaluate music.  
— Anchor Standard 7

>❝ I can examine music from a variety of stylistic and historical periods and cultures.  
— Anchor Standard 8

>❝ I can relate music to other arts disciplines, other subjects, and career paths.  
— Anchor Standard 9
