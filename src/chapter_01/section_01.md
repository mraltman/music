1.1. Artistic Processes
================================

*The four top-level, overlapping categories of Standards for Music at all levels.*

---
<img src=".artistic_processes.svg" style="float:right;margin:-2rem;margin-right:1rem;">

**Create** — *Use the elements of music to communicate new musical ideas and works.*

**Perform** — *Perform a variety of music with fluency and expression.*

**Respond** — *Respond to musical ideas as a performer and listener.*

**Connect** — *Relate music ideas to personal meaning, and to other arts disciplines and content areas.*
