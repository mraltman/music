### Resting Tone  
A _resting tone_ is an orienting pitch for a tonality. A piece of music can be said to be "in [some] tonality" because it establishes one pitch as the resting tone. This is meaningful in asymmetrical tonalities (such as Major, or any other that uses the diatonic or pentatonic collections). It is less meaningful in symmetrical tonalities (such as Whole Tone or Octotonic).  

_Atonal_ music avoids establishing any resting tone.  
