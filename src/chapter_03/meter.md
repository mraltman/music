### Meter  
Indicated by a time signature, meter describes the rhythmic parameters of a piece of music.  

+ specificity
+ beat
    + macro
    + micro

By far the most common meter is duple, followed by triple.  

**duple** &mdash; 
**triple**
**paired**

+ duple  
+ triple  
+ 2 + 3 (riba-dibity)   
+ 2 + 2	+ 3 (riba-diba-dibity)   
+ 3 + 2 (dibity-riba)  
