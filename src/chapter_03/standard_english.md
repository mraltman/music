### Standard English  

+ Language
    + a super-set of mutually intelligible dialects

+ Dialect
    + distinct from other dialects of a language  
    + defined according to a set of language features

+ Idiolect
    + enables a speaker to communicate  
    + a unique sub-set of features  
