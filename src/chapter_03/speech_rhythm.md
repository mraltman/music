### Speech Rhythm  
Unmetered music, where the rhythm comes directly from the text (rather than being imposed on it).  
