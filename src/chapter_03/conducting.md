### Watch the Conductor  
This is a particular concern. It is addressed early and consistently by having the class echo the conductor, waiting for the visual cues to breathe and echo. The conductor is usually the teacher but having students conduct is incorporated into activities as well _(see GM.CR NL.1)._  
