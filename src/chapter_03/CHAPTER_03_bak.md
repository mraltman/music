Chapter 3 — Concept Guides
==========================


*Being a curated slice of the South Carolina College- and Career-Ready (CCR) Standards for General Music Proficiency at the novice levels, adapted, with accompanying material.*

---

### Contents  
Meter  
Movement  
Music Class Rules  
Pitches and Notes  
Resting Tone  
Rhythm  
Serialism  
Singing  
Solfege  
Sound-Before-Sign  
Speech Rhythm  
Standard English  
Symbols  
Tonalities  
Watch the Conductor  
Whole-Part-Whole: "The Hackburger"

### Meter  
Indicated by a time signature, meter describes the rhythmic parameters of a piece of music.  

+ specificity
+ beat
    + macro
    + micro

By far the most common meter is duple, followed by triple.  

**duple** &mdash; 
**triple**
**paired**

+ duple  
+ triple  
+ 2 + 3 (riba-dibity)   
+ 2 + 2	+ 3 (riba-diba-dibity)   
+ 3 + 2 (dibity-riba)  

### Movement &mdash; Laban  

> Effort, or what Laban sometimes described as dynamics, is a system for understanding subtle characteristics about movement with respect to inner intention. The difference between punching someone in anger and reaching for a glass is slight in terms of body organization &mdash; both rely on extension of the arm. </br></br> Laban distinguishes attention to strength, control, and timing of movement in four "effort factors", each
a sub-category of movement with two opposite polarities (Effort elements). </br>  </br>  From Wikipedia (Paraphrased)  


| Factor | With      | Against |
| :---   | :---      | ---:    |
| Space  | Indirect  | Direct  |
| Weight | Light     | Heavy   |
| Time   | Sustained | Sudden  |
| Flow   | Free      | Bound   |

### Music Class Rules  
0. Self Space  
0. One Talker  
0. Eyes Up!  

### Pitches and Notes  
+ high vs. low  
+ solfège
	 + develop audiation  
	 + develop pattern recognition  

### Rhythm  
+ steady pulse  
	 + prefer _pulse_ to _beat_ because of language change  

### Resting Tone  
A _resting tone_ is an orienting pitch for a tonality. A piece of music can be said to be "in [some] tonality" because it establishes one pitch as the resting tone. This is meaningful in asymmetrical tonalities (such as Major, or any other that uses the diatonic or pentatonic collections). It is less meaningful in symmetrical tonalities (such as Whole Tone or Octotonic).  

_Atonal_ music avoids establishing any resting tone.  

### Singing  
+ warm ups  
   	 + full-range  
       	 + expand vocal range (physical) 
       	 + expand audiation range (mental)  
   	 + tongue and jaw control  
       	 + avoid unnecessary tension  
   	 + non-discrete pitch  
       	 + high - low, low - high  
       	 + moving pitches are easier to audiate    
+ breathing  
   	 + inhale  
       	 + much air (understand hyperventilation)  
       	 + abdominal, not clavical  
       	 + ribcage expansion (advanced)  
   	 + exhale  
       	 + controlled release
       	 + laryngeal onset (advanced)  
         
### Serialism  
_12-Tone Serialism_ is a composition technique that begins with all twelve notes of the chromatic scale arranged in a _row_. That row becomes the basis for composition &mdash; the composer is limited to that precise sequence of notes. The resulting music is unpredictable (by design), and usually dissonant with little to no sense of tonality.  

Other serial methods follow the same concept more loosely, with a smaller set of pitches or with more flexible rules governing their use; or more strictly, with other elements (e.g. rhythm) predetermined in rows as well. Why, you ask? Composers got bored, and it _can_ produce intellectually stimulating music.   

### Solfège
**Tonal Solfège** &mdash; Solfège usually refers to _Tonal Solfège,_ a descendant of the system featured in _The Sound of Music._ A symbolic system which assigns labels to standard divisions of musical pitch.  

**Rhythm Solfège** &mdash; A symbolic system e.g. _takadimi,_ which assigns labels to standard divisions of musical time.  

### Sound-Before-Sign  
According to this guideline, symbols for concepts (including names for concepts) are relatively unimportant, and should only be emphasized once the concept is understood. Symbols will follow concepts they represent only after students can discriminate between different instances of the concept. For instance, students will sing in Major and Dorian tonalities before learning the word "tonality". Sound-Before-Sign is a logical approach but it can lead to issues; namely, sometimes students never make it to the signs (cough, cough, Suzuki Method). To mitigate this pitfall in note reading in particular, students will begin to learn about notation early. All I require is that they have sung different pitches which can be identified as "high" and "low" _(GM.CR NL.1.1, 1.2)._  

It should be noted that SBS appears to conflict with Bloom's Taxonomy, Level 1.  

### Speech Rhythm  
Unmetered music, where the rhythm comes directly from the text (rather than being imposed on it).  

### Standard English  
**TL;DR** &mdash; Particularities of language are closely tied to a person's identity and the way others view them, in both cases, for good or ill. Public education requires students learn Standard English, creating a disparity between students who speak Standard English at home and those who don't.   

If you're reading this please let me know, and I'll be glad to write this part next!  

There is good evidence to show students who use non-standard grammar at home have more success learning to use Standard English if usage differences are contrasted without an absolute value judgment. So just to be clear, Standard English is the goal. "Language" is a loaded word. It is not only a means of communication, it's also tied in to social factors. And it is precisely _because_ of those social factors, that we must set them aside to think about language clearly. Only then can we equip students to navigate the choppy waters of communicating in a hierarchical society.  

+ Language &mdash; Super-set
    + a super-set of mutually intelligible dialects

+ Dialect &mdash; Set  
    + a set of features common among idiolects  
    + distinct from other dialects of a language  

+ Idiolect &mdash; Sub-set  
    + a unique sub-set of features  
    + enables a speaker to communicate  

A Language is represented by its most prestigious dialect. In the case of English, that would be Standard English, though in fact there are many regional varieties of it, and the speakers of Standard English in any given region value conformity to some features more than others. For example, Americans place less importance on pronunciation and more importance on grammar. Evidently, that's not the case in England, where a certain pronunciation will help a speaker avoid scorn while a grammatical nonconformity might pass without notice (example forthcoming).  

From the point of view of someone with no social preference, differences among dialects of a language are fascinating, and one is no more "correct" than another. Each is a rule-governed system, they just have different sets of rules.  

### Symbols  
+ names of tonalities  
+ names of meters  
+ note reading  
	 + identifying "notes" in notation  
	 + "everything else is furniture"  

### Tonalities  
+ Major  
+ Dorian  
+ Phrygian  
+ Lydian  
+ Mixolydian  
+ Minor  

### Watch the Conductor  
This is a particular concern. It is addressed early and consistently by having the class echo the conductor, waiting for the visual cues to breathe and echo. The conductor is usually the teacher but having students conduct is incorporated into activities as well _(see GM.CR NL.1)._  

### Whole-Part-Whole: "The Hackburger"  
The "Hackburger" is a mixed metaphor representing a Whole-Part-Whole structure. The "bottom bun" and "top bun" are the repetitions of a _whole,_ usually a song or section of a song long enough to establish musical context. Learners sing the _whole_ (or listen actively if it's unfamiliar). The "hack" is the _part_, a tone pattern or other element to be targeted. It goes between the _wholes,_ like a burger goes between buns, and it does its work while the first _whole_ is still active in working memory. The _part_ must be something learners do; only listening is not effective. The second _whole_ follows immediately after, while the _part_ is still in working memory.

Using Whole-Part-Whole, we are intentionally changing the way our brains handle music (like computer programming, hence "the hack") &mdash; and that core is surrounded by  context (hence "hackburger buns") _(GM.CR NL.1)._  
