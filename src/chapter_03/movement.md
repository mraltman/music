### Movement &mdash; Laban  

> Effort, or what Laban sometimes described as dynamics, is a system for understanding subtle characteristics about movement with respect to inner intention. The difference between punching someone in anger and reaching for a glass is slight in terms of body organization &mdash; both rely on extension of the arm. </br></br> Laban distinguishes attention to strength, control, and timing of movement in four "effort factors", each
a sub-category of movement with two opposite polarities (Effort elements). </br>  </br>  From Wikipedia (Paraphrased)  


| Factor | With      | Against |
| :---   | :---      | ---:    |
| Space  | Indirect  | Direct  |
| Weight | Light     | Heavy   |
| Time   | Sustained | Sudden  |
| Flow   | Free      | Bound   |
