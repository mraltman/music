### Solfège
**Tonal Solfège** &mdash; Solfège usually refers to _Tonal Solfège,_ a descendant of the system featured in _The Sound of Music._ A symbolic system which assigns labels to standard divisions of musical pitch.  

**Rhythm Solfège** &mdash; A symbolic system e.g. _takadimi,_ which assigns labels to standard divisions of musical time.  
