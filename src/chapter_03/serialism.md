### Serialism  
_12-Tone Serialism_ is a composition technique that begins with all twelve notes of the chromatic scale arranged in a _row_. That row becomes the basis for composition &mdash; the composer is limited to that precise sequence of notes. The resulting music is unpredictable (by design), and usually dissonant with little to no sense of tonality.  

Other serial methods follow the same concept more loosely, with a smaller set of pitches or with more flexible rules governing their use; or more strictly, with other elements (e.g. rhythm) predetermined in rows as well. Why, you ask? Composers got bored, and it _can_ produce intellectually stimulating music.   
