### Singing  
+ warm ups  
   	 + full-range  
       	 + expand vocal range (physical) 
       	 + expand audiation range (mental)  
   	 + tongue and jaw control  
       	 + avoid unnecessary tension  
   	 + non-discrete pitch  
       	 + high - low, low - high  
       	 + moving pitches are easier to audiate    
+ breathing  
   	 + inhale  
       	 + much air (understand hyperventilation)  
       	 + abdominal, not clavical  
       	 + ribcage expansion (advanced)  
   	 + exhale  
       	 + controlled release
       	 + laryngeal onset (advanced)  
