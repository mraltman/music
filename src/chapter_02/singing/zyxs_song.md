The ZYXs Song
=============

*The ABCs Song, exactly backward.*

---


What Am I Supposed to Do with This?!
------------------------------------
Sing it, of course 😂

It's a puzzle.
I could tell you how to solve the puzzle,
but where's the fun in that? 😉
Look, you know the song, right?
You know the regular version (where it's not backward).
That's enough to get you started.

<img src="ZYXs.svg">

But Why Would You Do This?! 😑
---------------------------
### Why a Puzzle? 🤓
Do you mean why would I give you a puzzle to solve?
That's easy,
*it's for your brain.*
**There is no right way to solve this puzzle.**
There is a *correct answer,* of course,
but any path to the answer is fine.
You can work with other kids.
You can work with grown ups.
You can use any tools you think of.
Anything that gets you there — it's all fair.

I'm super curious how parents and other teachers would solve this puzzle.
(For someone who can read music well it isn't a puzzle, by the way.
It's just music notation.)
I will tell you this;
the notes *above* the words *go with* the words.
The notes *below* the words, you can ignore those.

We've talked in class about music notation — 
I'm famous for saying, "Look at the *notes*, everything else is *furniture*."
Notes are little pictures of sounds.
All the other stuff is just there to help out the notes — you can ignore it for now.

### No, Why Backward?! 🤨
Ohhhhhhhhhhh, you mean why would I sing the ABCs backward?

Okay, calm down,
I can explain. Story time.

I have always wanted to be able to say the ABCs backward.
One day, in a flash of genius,
I realized singing it was the way to go.
That's how we all learned the ABCs in the first place,
am I right?
(Also, I had done this two more times before;
I learned the Hebrew "alef bet" from an Israeli friend
and I learned the Greek "alpha beta" by writing my own song for it.)

I'm a composer too,
and I *teach* composition,
so I thought, "Going through some notes backward is a composition hack.
What happens if I just do that?" 🤔

**The Answer? IT'S AWESOME.**

I got lucky with this one. *Going through some notes backward*
is one of many tools composers use to write new music.
In this case it completely changed the mood of the song,
and it was so good,
my job was done.
It turns out the ABCs song is way better backward 😅

Now I know my ZYXs, me with sing you won't time next?

