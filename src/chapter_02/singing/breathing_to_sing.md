Breathing to Sing
=================

*This should be moved into a Concept Guide.*

---

### Problems
**Lore**  

+ "diaphragmatic breathing"  

Well *actually* your diaphragm is like your heart;
it isn't under your control,
so it's basically a waste of time to talk about "breathing with your diaphragm".

+ national schools *(Italian, German, etc)*

**Getting Light Headed**

This comes from having too much oxygen in your blood.
When someone has a panic attack,
it's helpful to have them breathe into *and from* a paper bag.
In that case,
since they can't make themselves stop breathing in so much,
the paper bag is a hack to control the amount of oxygen that comes in.

+ give it a few seconds and it'll pass
+ you might notice your body stops automatic breathing 😬 

### Solutions

+ breathe out first
+ shoulders back, chest up
+ poke out your tongue
+ squat 😂
